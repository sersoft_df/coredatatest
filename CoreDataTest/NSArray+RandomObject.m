//
//  NSArray+RandomObject.m
//  CoreDataTest
//
//

#import "NSArray+RandomObject.h"

@implementation NSArray (RandomObject)

- (id)randomObject {
    NSUInteger randomIndex = arc4random() % self.count;
    return self[randomIndex];
}

@end
