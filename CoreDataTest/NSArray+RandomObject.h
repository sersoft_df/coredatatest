//
//  NSArray+RandomObject.h
//  CoreDataTest
//
//

#import <Foundation/Foundation.h>

@interface NSArray (RandomObject)

@property (nonatomic, strong, readonly) id randomObject;

@end
