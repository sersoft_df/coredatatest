//
//  ViewController.h
//  CoreDataTest
//
//

#import <UIKit/UIKit.h>
#import "SimpleGraphView.h"

@interface ViewController : UIViewController

@property (nonatomic, weak) IBOutlet UILabel *textLabel;
@property (nonatomic, weak) IBOutlet UIButton *button;
@property (weak, nonatomic) IBOutlet UILabel *memoryLabel;
@property (weak, nonatomic) IBOutlet SimpleGraphView *graphView;

- (IBAction)buttonPressed:(id)sender;

@end

