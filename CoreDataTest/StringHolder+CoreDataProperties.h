//
//  StringHolder+CoreDataProperties.h
//  CoreDataTest
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "StringHolder.h"

NS_ASSUME_NONNULL_BEGIN

@interface StringHolder (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *text;
@property (nullable, nonatomic, retain) NSString *key;

@end

NS_ASSUME_NONNULL_END
