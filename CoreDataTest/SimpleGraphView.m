//
//  SimpleGraphView.m
//  CoreDataTest
//
//  Created by Daniel Friedrich on 11.12.15.
//  Copyright © 2015 ser.soft GmbH. All rights reserved.
//

#import "SimpleGraphView.h"

@implementation SimpleGraphView

@synthesize yValues;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor lightGrayColor];
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    [super drawRect:rect];
    CGRect insetRect = CGRectInset(self.frame, 10, 15);
    double maxSpeed = [[yValues valueForKeyPath:@"@max.doubleValue"] doubleValue];
    CGFloat yRatio = insetRect.size.height / maxSpeed;
    CGFloat xRatio = (yValues.count == 1) ? 0 : (insetRect.size.width / (yValues.count-1));
    UIBezierPath *sparkline = [UIBezierPath bezierPath];
    for (int x = 0; x < yValues.count; x++) {
        if (x == 0) {
            CGPoint newPoint = CGPointMake(x * xRatio, insetRect.size.height - (yRatio * [[yValues objectAtIndex:x] doubleValue] - insetRect.origin.y));
            [sparkline moveToPoint:newPoint];
        }
        else {
            CGPoint newPoint = CGPointMake(x * xRatio + insetRect.origin.x, insetRect.size.height - (yRatio * [[yValues objectAtIndex:x] doubleValue] - insetRect.origin.y));
            [sparkline addLineToPoint:newPoint];
        }
    }
    [[UIColor redColor] set];
    [sparkline stroke];
}

@end