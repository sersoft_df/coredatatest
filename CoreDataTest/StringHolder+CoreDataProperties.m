//
//  StringHolder+CoreDataProperties.m
//  CoreDataTest
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "StringHolder+CoreDataProperties.h"

@implementation StringHolder (CoreDataProperties)

@dynamic text;
@dynamic key;

@end
