//
//  ViewController.m
//  CoreDataTest
//
//

#import "ViewController.h"
#import "AppDelegate.h"
#import "StringHolder.h"
#import "NSArray+RandomObject.h"
#import <mach/mach.h>

@interface ViewController ()
@property (nonatomic, strong, readonly) NSArray *keys;
@property (nonatomic, strong, readonly) AppDelegate *appDelegate;
@property (nonatomic, strong) dispatch_queue_t queue;
@property (nonatomic, strong) NSManagedObjectContext *context;
@property (nonatomic, strong) NSNumberFormatter *numberFormatter;
@property (nonatomic, strong) NSMutableArray *chartValues;

@end

@implementation ViewController
@synthesize keys = _keys;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.queue = dispatch_queue_create("CD_TEST", NULL);
    [self.button setTitle:@"Start" forState:UIControlStateNormal];
    self.textLabel.text = @"";
    self.memoryLabel.text = @"";
    
    /** Initialize the number-formatter **/
    self.numberFormatter = [[NSNumberFormatter alloc] init];
    self.numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    self.numberFormatter.formatterBehavior = NSNumberFormatterBehavior10_4;
    self.numberFormatter.maximumFractionDigits = 0;
    self.numberFormatter.minimumFractionDigits = 0;

    
    self.chartValues = [[NSMutableArray alloc] init];
    self.graphView.yValues = self.chartValues;

    //[self report_memory];
}

- (void)buttonPressed:(id)sender {
    self.button.enabled = NO;
    [self startLoadingTexts];
}

- (void)startLoadingTexts {
    dispatch_async(self.queue, ^{
        for (NSUInteger i = 0; i < 20000; i++) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSString *key = self.keys.randomObject;
                NSString *str = [self findTextWithKey:key];
                self.textLabel.text = str;
                if ((i % 10) == 0) {
                    [self report_memory];
                }
            });
            [NSThread sleepForTimeInterval:0.05];
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            self.button.enabled = YES;
        });
    });
}

- (NSString *)findTextWithKey:(NSString *)key {
    __block NSString *result = @"n/a";
    NSManagedObjectContext *ctx = [self createContextWithConcurrencyType:NSMainQueueConcurrencyType];
    [ctx performBlockAndWait:^{
        NSFetchRequest *req = [[NSFetchRequest alloc] initWithEntityName:@"StringHolder"];
        req.predicate = [NSPredicate predicateWithFormat:@"key == %@", key];
        NSArray *results = [ctx executeFetchRequest:req error:nil];
        result = [results.firstObject text];
    }];
    return result;
}

- (NSManagedObjectContext *)createContextWithConcurrencyType:(NSManagedObjectContextConcurrencyType)ct {
    if (_context == nil) {
        _context = [[NSManagedObjectContext alloc] initWithConcurrencyType:ct];
        _context.undoManager = nil;
        _context.persistentStoreCoordinator = self.appDelegate.localizationPSC;
    }
    return self.context;
}

- (AppDelegate *)appDelegate {
    return [UIApplication sharedApplication].delegate;
}

- (NSArray *)keys {
    return self.appDelegate.samples.allKeys;
}

- (void)report_memory {
    static mach_vm_size_t last_resident_size=0;
    static mach_vm_size_t greatest = 0;
    static mach_vm_size_t last_greatest = 0;
    
    struct mach_task_basic_info info;
    mach_msg_type_number_t size = MACH_TASK_BASIC_INFO_COUNT;
    kern_return_t kerr = task_info(mach_task_self(),
                                   MACH_TASK_BASIC_INFO,
                                   (task_info_t)&info,
                                   &size);
    if( kerr == KERN_SUCCESS ) {
        int diff = (int)info.resident_size - (int)last_resident_size;
        mach_vm_size_t latest = info.resident_size;
        if( latest > greatest   )   greatest = latest;  // track greatest mem usage
        NSString *diffStr = [self.numberFormatter stringFromNumber:@(diff / 1024)];
        NSString *sizeStr = [self.numberFormatter stringFromNumber:@(info.resident_size / 1024)];
        NSString *msg = [NSString stringWithFormat:@"Mem (kB): %@\nDifference (kB): %@", sizeStr, diffStr];
        self.memoryLabel.text = msg;
        if (info.resident_size > 0) {
            [self.chartValues addObject:@(info.resident_size / 1024)];
            [self.graphView setNeedsDisplay];
        }
    } else {
        NSLog(@"Error with task_info(): %s", mach_error_string(kerr));
    }
    last_resident_size = info.resident_size;
    last_greatest = greatest;
}

@end
