//
//  SimpleGraphView.h
//  CoreDataTest
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SimpleGraphView : UIView {
    NSArray *yValues;
}

@property (strong, atomic) NSArray *yValues;

@end
