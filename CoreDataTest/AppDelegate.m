//
//  AppDelegate.m
//  CoreDataTest
//
//

#import "AppDelegate.h"
#import "StringHolder.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if (![ud boolForKey:@"samplesInserted"]) {
        [ud setBool:YES forKey:@"samplesInserted"];
        [self insertTestRecords];
    }

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
}

- (void)insertTestRecords {
    NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    context.undoManager = nil;
    context.persistentStoreCoordinator = self.localizationPSC;
    [context performBlockAndWait:^{
        NSEntityDescription *description = [NSEntityDescription entityForName:@"StringHolder"
                                                       inManagedObjectContext:context];

        [self.samples enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL * stop) {
            StringHolder *object = [[StringHolder alloc] initWithEntity:description
                                         insertIntoManagedObjectContext:context];
            object.key = key;
            object.text = obj;
        }];
    }];
    [context performBlockAndWait:^{
        [context save:nil];
    }];
}

@synthesize samples = _samples;
- (NSDictionary *)samples {
    if (!_samples) {
        _samples = @{
                     @"Checking for available updates..." : @"Suche nach verfügbaren Updates...",
                     @"Checking for additional changes..." : @"Suche nach weiteren Änderungen...",
                     @"Checking for available Apple Software Updates..." : @"Suche nach neuen Apple Software Updates...",
                     @" (will be removed)" : @" (wird entfernt)",
                     @"\nAll pending updates will be installed. Unsaved work will be lost.\nYou may avoid the forced logout by logging out now." : @"\nAlle anstehenden Aktualisierungen werden installiert. Alle ungespeicherte Arbeit geht verloren.\nDurch jetziges Abmelden können sie eine erzwungene Abmeldung verhindern",
                     @"A logout is recommended before updating. Please be patient as there may be a short delay at the login window. Log out and update now?" : @"Für die Aktualisierung ist ein Abmelden empfohlen. Es kann zu kurzen Verzögerungen am Anmeldebildschirm kommen, bitte gedulden sie sich einen Moment. Jetzt abmelden und aktualisieren?",
                     @"A logout is required before updating. Please be patient as there may be a short delay at the login window. Log out and update now?" : @"Für die Aktualisierung ist ein Abmelden erforderlich. Es kann zu kurzen Verzögerungen am Anmeldebildschirm kommen, bitte gedulden sie sich einen Moment. Jetzt abmelden und aktualisieren?",
                     @"A logout will be forced at approximately %s." : @"Eine Abmeldung wird in ca. %s erzwungen.",
                     @"A logout will be forced in less than %s minutes." : @"Eine Abmeldung wird in weniger als %s Minuten erzwungen.",
                     @"A logout will be forced in less than a minute.\nAll pending updates will be installed. Unsaved work will be lost." : @"Eine Abmeldung wird in weniger als einer Minute erzwungen.\nAlle anstehenden Aktualisierungen werden installiert. Alle ungespeicherte Arbeit geht verloren.",
                     @"A restart is required after updating. Please be patient as there may be a short delay at the login window. Log out and update now?" : @"Für die Aktualisierung ist ein Neustart erforderlich. Es kann zu kurzen Verzögerungen am Anmeldebildschirm kommen, bitte gedulden sie sich einen Moment. Jetzt abmelden und aktualisieren?",
                     @"Cancel" : @"Abbrechen",
                     @"Cannot check for updates" : @"Auf Aktualisierungen kann nicht geprüft werden",
                     @"Cannot start installation session" : @"Installation kann nicht gestartet werden",
                     @"Check again" : @"Erneut prüfen",
                     @"Conflicting applications running" : @"Es gibt einen Konflikt mit laufenden Programmen",
                     @"Continue" : @"Fortsetzen",
                     @"For best results, you should connect your computer to a power source before updating. Are you sure you want to continue the update?" : @"Sie sollten den Computer an eine Stromquelle anschliessen bevor Sie die Aktualisierung durchführen. Sind Sie sicher, dass Sie die Aktualisierung fortsetzen möchten?",
                     @"Forced Logout for Mandatory Install" : @"Erzwungene Abmeldung für eine zwingend notwendige Installation",
                     @"Install session failed" : @"Installation fehlgeschlagen",
                     @"Installed" : @"Installiert",
                     @"Log out and update" : @"Abmelden und aktualisieren",
                     @"Log out and update now" : @"Jetzt abmelden und aktualisieren",
                     @"Logout Recommended" : @"Abmelden empfohlen",
                     @"Logout Required" : @"Abmelden erforderlich",
                     @"Logout will be required." : @"Abmelden ist erforderlich.",
                     @"Mandatory Updates Pending" : @"Zwingend notwendige Aktualisierungen anstehend",
                     @"Managed Software Update cannot contact the update server at this time.\nIf this situation continues, contact your systems administrator." : @"Geführte Softwareaktualisierung erreicht den Server nicht.\nWenn dieses Problem weiter besteht, informieren Sie bitte Ihren Administrator.",
                     @"Managed Software Update failed its preflight check.\nTry again later." : @"Geführte Softwareaktualisierung meldet einen Fehler beim Preflight.\nProbieren Sie es später noch einmal.",
                     @"Not installed" : @"Nicht installiert",
                     @"Not removable" : @"Nicht entfernbar",
                     @"OK" : @"OK",
                     @"One or more mandatory updates are overdue for installation. A logout will be forced soon." : @"Ein oder mehrere zwingend notwendige Aktualisierungen sind überfällig. Eine Abmeldung wird in Kürze erzwungen.",
                     @"One or more updates must be installed by %s. A logout may be forced if you wait too long to update." : @"Ein oder mehrere Aktualisierungen müssen unbedingt bis %s installiert werden. Eine Abmeldung wird möglicherweise erzwungen, wenn Sie zu lange mit der Aktualisierung warten.",
                     @"Optional software..." : @"Optionale software...",
                     @"Other users logged in" : @"Andere User sind eingeloggt",
                     @"Quit" : @"Beenden",
                     @"Restart" : @"Neustart",
                     @"Restart Required" : @"Neustart erforderlich",
                     @"Restart will be required." : @"Ein Neustart ist erforerlich.",
                     @"Scheduled removal of managed software." : @"Geplantes entfernen von verwalteter Software",
                     @"Show updates" : @"Zeige Aktualisierungen",
                     @"Software installed or removed requires a restart. You will have a chance to save open documents." : @"Softwareaktualisierung oder -entfernung benötigt einen Neustart. Sie können offene Dokumente noch sichern.",
                     @"Software removals" : @"Software entfernen",
                     @"Starting…" : @"Starten…",
                     @"There are other users logged into this computer.\nUpdating now could cause other users to lose their work.\n\nPlease try again later after the other users have logged out." : @"Es sind andere Benutzer angemeldet\nEine Aktualisierung könnte zu Datenverlust führen.\n\nBitte probieren Sie es, wenn die anderen abgemeldet sind, noch einmal.",
                     @"There is a configuration problem with the managed software installer. Could not start the install session. Contact your systems administrator." : @"Managed Software Installer hat ein Konfigurationsproblem (Could not start the install session). Informieren Sie Ihren Administrator.",
                     @"There is a configuration problem with the managed software installer. Could not start the process. Contact your systems administrator." : @"Managed Software Installer hat ein Konfigurationsproblem (Could not start the process). Informieren Sie Ihren Administrator.",
                     @"There is a configuration problem with the managed software installer. Could not start the update check process. Contact your systems administrator." : @"Managed Software Installer hat ein Konfigurationsproblem (Could not start the update check process). Informieren Sie Ihren Administrator.",
                     @"There is a configuration problem with the managed software installer. The process ended unexpectedly. Contact your systems administrator." : @"Managed Software Installer hat ein Konfigurationsproblem (The process ended unexpectedly). Informieren Sie Ihren Administrator.",
                     @"There is no new software for your computer at this time." : @"Zurzeit sind keine Softwareaktualisierungen für Ihren Computer verfügbar.",
                     @"This item must be installed by " : @"Diese Software musst installiert sein am ",
                     @"This item will be removed." : @"Diese Software wird entfernt",
                     @"Update available" : @"Aktualisierungen verfügbar",
                     @"Update check failed" : @"Prüfung auf Aktualisierungen fehlgeschlagen",
                     @"Update later" : @"Später Aktualisieren",
                     @"Update without logging out" : @"Aktualisieren ohne abmelden",
                     @"Will be installed" : @"wird installiert",
                     @"Will be removed" : @"wird entfernt",
                     @"Will be updated" : @"wird aktualisiert",
                     @"Will not be installed" : @"wird nicht installiert",
                     @"Will not be removed" : @"wird nicht entfernt",
                     @"You must quit the following applications before proceeding with installation:\n\n%s" : @"Die folgenden Programme müssen geschlossen werden, um mit der Aktualisierung fortfahren zu können:\n\n%s",
                     @"Your computer is not connected to a power source." : @"Ihr Computer ist nicht an eine Stromquelle angeschlossen.",
                     @"Your software is up to date." : @"Ihre Software ist auf dem neuesten Stand."
                     };
    }
    return _samples;
}

#pragma mark - Core Data stack

@synthesize managedObjectModel = _managedObjectModel;
@synthesize otherPSC = _otherPSC;
@synthesize localizationPSC = _localizationPSC;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.sersoft.CoreDataTest" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"CoreDataTest" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)otherPSC {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_otherPSC != nil) {
        return _otherPSC;
    }

    // Create the coordinator and store

    _otherPSC = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"CoreDataTest.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_otherPSC addPersistentStoreWithType:NSSQLiteStoreType configuration:@"Other" URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }

    return _otherPSC;
}

- (NSPersistentStoreCoordinator *)localizationPSC {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_localizationPSC != nil) {
        return _localizationPSC;
    }

    // Create the coordinator and store

    _localizationPSC = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"CoreDataTestLoc.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_localizationPSC addPersistentStoreWithType:NSSQLiteStoreType configuration:@"Localization" URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }

    return _localizationPSC;
}

@end
