//
//  SomeOtherObject+CoreDataProperties.h
//  CoreDataTest
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "SomeOtherObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface SomeOtherObject (CoreDataProperties)


@end

NS_ASSUME_NONNULL_END
