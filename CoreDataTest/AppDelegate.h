//
//  AppDelegate.h
//  CoreDataTest
//
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (nonatomic, strong, readonly) NSDictionary *samples;
@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *otherPSC;
@property (nonatomic, strong, readonly) NSPersistentStoreCoordinator *localizationPSC;

@property (nonatomic, strong, readonly) NSURL *applicationDocumentsDirectory;


@end

